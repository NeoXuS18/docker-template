#!/bin/bash

# Variable pour stocker le PID du processus Java
pid_file="pid.txt"

# Fonction pour lancer l'application
function run_application() {
    mvn clean package -DskipTests
    java -jar target/api-0.0.1-SNAPSHOT.jar &
    echo $! > "$pid_file"
}

# Vérification si le fichier PID existe et le processus est en cours d'exécution
if [ -f "$pid_file" ] && kill -0 "$(cat "$pid_file")"; then
    echo "L'application est déjà en cours d'exécution."
else
    # Suppression du fichier PID précédent s'il existe
    rm -f "$pid_file"
    
    # Lancement de l'application
    run_application
fi

# Surveillance des fichiers
while true; do
    # Vérification des modifications dans le répertoire de l'application
    current_modified=$(find src/main -type f -exec stat -c %Y {} \; | sort | tail -n 1)
    if [[ -n "$last_modified" ]] && [[ "$current_modified" -gt "$last_modified" ]]; then
        echo "Fichiers modifiés. Arrêt de l'application..."
        
        # Arrêt du processus Java en utilisant le PID enregistré dans le fichier
        if [ -f "$pid_file" ]; then
            kill "$(cat "$pid_file")"
            rm -f "$pid_file"
        fi
        
        echo "Relancement de l'application..."
        run_application
    fi
    last_modified=$current_modified
    sleep 1
done


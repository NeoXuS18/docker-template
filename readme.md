# DOCKER TEMPLATE

> Template de docker en mode dev pour une API en Spring Boot avec une base de données en Postgres ainsi qu'un front-end en React.


[![Version](https://img.shields.io/badge/version-1.0.3-blue.svg)](https://semver.org/)


Vous avez trois possibilités pour lancer votre Docker. Dans un premier temps, si vous souhaitez lancer tous les services :
```shell
$ docker compose -f "docker-compose.dev.yml" build
$ docker compose -f "docker-compose.dev.yml" up
```
Si vous souhaitez lancer le service de l'API avec sa base de données :

```shell
$ docker compose -f "docker-compose.back.yml" build
$ docker compose -f "docker-compose.back.yml" up
```

Si vous souhaitez lancer le service React :

```shell
$ docker compose -f "docker-compose.front.yml" build
$ docker compose -f "docker-compose.front.yml" up
```

Lorsque vous souhaitez arrêter vos conteneurs :

```shell
Ctrl + C
$ docker compose -f "votre nom de docker compose .yml" down
```


© 2023 Lopez Antonin. Tous droits réservés. Veuillez noter que toutes les images utilisées dans ce projet sont la propriété de Lopez Antonin et leur utilisation dans d'autres projets nécessite une citation appropriée et l'autorisation de l'auteur.

Pour plus d'informations, veuillez consulter les liens suivants :




<div class="image-container">
   <a href="https://gitlab.com/NeoXuS18">
   <img src="https://about.gitlab.com/images/press/press-kit-icon.svg" alt="Logo GitLab" width="20px">
   </a>
   <a href="https://www.linkedin.com/in/antonin-lopez-8a9210242/">
   <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/c/ca/LinkedIn_logo_initials.png/600px-LinkedIn_logo_initials.png" alt="Logo Linkedin" width="20px">
   </a>
</div>
